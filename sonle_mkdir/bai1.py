from __future__ import division
import re
import httpagentparser
import operator
from collections import OrderedDict


file =  open('/home/son-le/Desktop/gistfile1.txt', 'r')

string = file.read()
array = string.splitlines()

lineindex = 1

array_browser = []
browser_dict = {}

number_line_success = 0
number_line_with_error = 0
total = 0

for x in array:
	# result = re.match(r'(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}) (\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}) (\[[0-9a-zA-Z\-\+\:]+\]) ([a-zA-Z0-9]+\.[a-zA-Z]{1,3}) (\".*\") (\d{1,3}) (\d+) (\".*\") (\".*\") (\-) (\d+) (\d+\.\d+)', x)
	try:
		browser = httpagentparser.simple_detect(x)[1]
		if browser in array_browser:
			browser_dict[browser] += 1
		else:
			array_browser.append(browser)
			browser_dict[browser] = 1
		number_line_success += 1
	except Exception as e:
		number_line_with_error += 1
	total += 1

#
# browser_dict_sorted = OrderedDict(sorted(browser_dict.items(),key=operator.itemgetter(1),reverse=True))
#
# for name, count in browser_dict_sorted.iteritems():
# 	print ("{:40s} {:<10d}({:0.5%})".format(name,count,count/number_line_success))
browser_ssl = OrderedDict(sorted(browser_dict.iteritems(), key=operator.itemgetter(1), reverse=True))
for name, count in browser_ssl.items():
	print ('{:40s}{:10d}{:0.5}').format(name,count,count/number_line_success)